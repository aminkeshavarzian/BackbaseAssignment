package com.backbase.backbaseassignment;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Amin on 21/03/2018.
 *
 * this class will provide or data and everything we do with or Json list is done here
 * _ reading json file
 * _ parsing json to Java Objects
 * _ searching through list
 * _ list pagination
 *
 * and since we want it to be persistent through Application life, we made it Singleton
 *
 */

public class DataProvider {

    private static DataProvider INSTANCE;
    private String jsonData;
    private String lastSearchPhrase = "";
    private List<CityModel> results;
    private static List<CityModel> cities;

    static synchronized DataProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataProvider(getStringFromFile());
        }
        return (INSTANCE);
    }

    static void setINSTANCE(DataProvider INSTANCE) {
        DataProvider.INSTANCE = INSTANCE;
    }

    DataProvider(String jsonData) {
        this.jsonData = jsonData;
    }

    List<CityModel> getCities() {
        if (cities == null) {
            if (jsonData == null){
                jsonData = getStringFromFile();
            }
            Type listType = new TypeToken<ArrayList<CityModel>>() {
            }.getType();
            cities = new Gson().fromJson(jsonData, listType);
            jsonData = null;
            Collections.sort(cities, new CityModel());
        }
        return cities;
    }

    List<CityModel> getCities(String searchPhrase, int page) {
        int start = page * CitiesListFragment.pageSize;
        int end = (page + 1) * CitiesListFragment.pageSize;
        if (start > getCities().size()){
            return null;
        }
        if (searchPhrase == null || searchPhrase.isEmpty()){
            lastSearchPhrase = "";
            return new ArrayList<>(getCities().subList(start, end > getCities().size() ? getCities().size() : end));
        }

        if (results == null || !searchPhrase.toLowerCase().equals(lastSearchPhrase.toLowerCase())) {
            searchPhrase = searchPhrase.toLowerCase();
            results = new ArrayList<>();
            for (CityModel cityModel : getCities()) {
                if (cityModel.getCountry().toLowerCase().equals(searchPhrase)
                        || cityModel.getName().toLowerCase().startsWith(searchPhrase)) {
                    results.add(cityModel);
                }
            }
            lastSearchPhrase = searchPhrase;
        }
        if (start > results.size()) return new ArrayList<>();
        return new ArrayList<>(results.subList(start, end > results.size() ? results.size() : end));
    }

    public static void setCities(List<CityModel> cities) {
        DataProvider.cities = cities;
    }

    private static String getStringFromFile() {
        try {
            Resources res = AppController.app.getResources();
            InputStream inputStream = res.openRawResource(R.raw.cities);
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            return new String(bytes);
        } catch (Exception e) {
            return "error";
        }
    }
}
