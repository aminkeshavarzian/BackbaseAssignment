package com.backbase.backbaseassignment;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Amin on 09/08/2016.
 * <p>
 * This class will be used for
 */

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.GroupViewHolder> {

    private List<CityModel> modelList;
    private OnCityClickListener cityClickListener;

    CityListAdapter(List<CityModel> modelList, OnCityClickListener cityClickListener) {
        this.modelList = modelList;
        this.cityClickListener = cityClickListener;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_city, viewGroup, false);
        return new GroupViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {
        final CityModel model = modelList.get(position);
        holder.city.setText(model.getName() + ",");
        holder.country.setText(model.getCountry());
        holder.itemCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityClickListener.onCityClicked(model);
            }
        });

    }

    static class GroupViewHolder extends RecyclerView.ViewHolder {

        View itemCV;
        TextView city, country;

        GroupViewHolder(View itemView) {
            super(itemView);
            itemCV = itemView.findViewById(R.id.item);
            city = itemView.findViewById(R.id.cityName);
            country = itemView.findViewById(R.id.countryCode);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
