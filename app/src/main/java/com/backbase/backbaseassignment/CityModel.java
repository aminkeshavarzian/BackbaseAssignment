package com.backbase.backbaseassignment;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by Amin on 21/03/2018.
 *
 */

public class CityModel implements Comparator<CityModel> {

    private String country;
    private String name;
    private LatLong coord;

    @SerializedName("_id")
    private
    String id;

    String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    LatLong getCoord() {
        return coord;
    }

    public void setCoord(LatLong coord) {
        this.coord = coord;
    }

    @Override
    public int compare(CityModel cityModel, CityModel t1) {
        int compare = cityModel.getName().compareToIgnoreCase(t1.getName());
        if (compare== 0){
            return cityModel.getCountry().compareTo(t1.getCountry());
        } else {
            return compare;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public class LatLong {
        double lon;
        double lat;

        double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }

        double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }
    }

}