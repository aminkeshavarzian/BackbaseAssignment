package com.backbase.backbaseassignment;

/**
 * Created by Amin on 22/03/2018.
 */

public interface OnCityClickListener {

    public void onCityClicked(CityModel cityModel);

}
