package com.backbase.backbaseassignment;

import android.app.Application;

/**
 * Created by Amin on 21/03/2018.
 *
 * extending the Application class, this class will be useful for creating a Application wide resources such
 * as Volley Network Request Queue.
 * as well as performing some logic before App starts any activity
 */

public class AppController extends Application {

    public static AppController app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static synchronized AppController getInstance() {
        return app;
    }
}
