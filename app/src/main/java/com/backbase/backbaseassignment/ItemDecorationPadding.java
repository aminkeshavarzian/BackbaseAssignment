package com.backbase.backbaseassignment;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Amin on 22/03/2018.
 *
 * used for adding extra paddingTop for the first item in recyclerView
 */

public class ItemDecorationPadding extends RecyclerView.ItemDecoration {

    private int paddingTop;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int itemPosition = parent.getChildAdapterPosition(view);
        int itemCount = state.getItemCount();
        if (itemPosition == RecyclerView.NO_POSITION) {
            return;
        }
        if (itemPosition < 1) {
            outRect.set(view.getPaddingLeft(), dpToPixels(paddingTop), view.getPaddingRight(), view.getPaddingBottom());
        } else if (itemCount > 0 && itemPosition == itemCount - 1) {
//            outRect.set(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), dpToPixels(50f));
            outRect.set(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        } else {
            outRect.set(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        }
    }

    public ItemDecorationPadding(int paddingTop) {
        this.paddingTop = paddingTop;
    }

    private int dpToPixels(float dp) {
        final float scale = AppController.getInstance().getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }
}
