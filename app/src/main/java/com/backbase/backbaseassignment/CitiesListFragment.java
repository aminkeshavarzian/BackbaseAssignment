package com.backbase.backbaseassignment;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;

/**
 * Created by Amin on 22/03/2018.
 *
 * This Fragment will be used for displaying list of cities...
 * a lot of UI and logic codes are here, since the purpose of assignment was just to have a fast responsive
 * UI, I didn't spend time to seprate all the logic and UI
 */

public class CitiesListFragment extends Fragment implements OnCityClickListener {

    // the size of list before loading next page
    public static final int pageSize = 80;

    private List<CityModel> modelListToShow;
    private RecyclerView recyclerView;
    private CityListAdapter adapter;
    LinearLayoutManager layoutManager;
    View progress;

    // used for loadMore functionality
    private boolean isLoadingMore = false;
    private boolean isFinished = false;
    int visibleItemCount;
    int pastVisibleItems;
    int totalItemCount;
    int page = 0;

    String searchPhrase = null;

    boolean isWorking = false;

    // used for keeping the latest task
    ListLoadTask waitingTask;
    private EditText searchEt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        progress = view.findViewById(R.id.progress);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new ItemDecorationPadding(65));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        modelListToShow = DataProvider.getInstance().getCities(null, 0);
        adapter = new CityListAdapter(modelListToShow, this);
        recyclerView.setAdapter(adapter);
        setupUI(getActivity(), recyclerView);
        searchEt = view.findViewById(R.id.searchEt);
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // to avoid concurrent modification exception which happens when load more and
                // search happens at the same time
                recyclerView.stopScroll();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                search(editable.toString());
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0 && !isLoadingMore && !isFinished && !isWorking) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        isLoadingMore = true;
                        loadMore();
                    }
                }
            }

        });
        showHideList(true);
    }

    /*
    when app is already searching in data, we only need to keep the latest phrase
    entered by user, so when the current task is done, app will perform search for the
    latest search phrase.
    we could have also canceled the current task and perform tne new search phrase,
    but it would take too long to implement that since we have check in "doInBackground" if the
    task is canceled or not
    */
    private void search(String searchPhrase) {
        if (!isWorking && !isLoadingMore) {
            new ListLoadTask(searchPhrase).execute();
        } else {
            waitingTask = new ListLoadTask(searchPhrase);
        }
    }

    private void loadMore() {
        page++;
        List<CityModel> loadMoreList = DataProvider.getInstance().getCities(searchPhrase, page);
        if (loadMoreList == null || loadMoreList.size() == 0) {
            //means there was no extra cities to show
            isFinished = true;
        } else {
            final int itemCount = modelListToShow.size();
            modelListToShow.addAll(loadMoreList);
            Handler handler = new Handler();
            final Runnable runnable = new Runnable() {
                public void run() {
                    adapter.notifyItemRangeInserted(itemCount, modelListToShow.size());
                }
            };
            handler.post(runnable);
        }
        isLoadingMore = false;
    }

    // this method is used to show or hide list with fade effect and also show the progressbar
    private void showHideList(boolean showOrHide) {
        ObjectAnimator anim;
        if (showOrHide && recyclerView.getAlpha() < 1) {
            anim = ObjectAnimator.ofFloat(recyclerView, View.ALPHA, 0f, 1f);
            progress.setVisibility(View.GONE);
        } else if (!showOrHide && recyclerView.getAlpha() > 0) {
            anim = ObjectAnimator.ofFloat(recyclerView, "alpha", 1f, 0f);
            progress.setVisibility(View.VISIBLE);
        } else {
            return;
        }
        anim.setDuration(100); // duration 3 seconds
        anim.start();
    }

    /*
    using this method, when ever user touch on screen anywhere that is not an instance
    of EditText, we hide the keyboard, which is used to make the most of the screen
    */
    public static void setupUI(final Activity activity, final View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    if (activity.getCurrentFocus() != null) {
                        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                        activity.getCurrentFocus().clearFocus();
                    }
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(activity, innerView);
            }
        }
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onCityClicked(CityModel cityModel) {
        assert getActivity() != null;
        ((OnCityClickListener) getActivity()).onCityClicked(cityModel);
    }


    private class ListLoadTask extends AsyncTask<String, String, String> {

        String searchPhrase;

        ListLoadTask(String searchPhrase) {
            this.searchPhrase = searchPhrase;
        }

        @Override
        protected String doInBackground(String... params) {
            CitiesListFragment.this.searchPhrase = searchPhrase;
            page = 0;
            modelListToShow = DataProvider.getInstance().getCities(searchPhrase, page);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            if (waitingTask == null) {
                adapter = new CityListAdapter(modelListToShow, CitiesListFragment.this);
                recyclerView.setAdapter(adapter);

                // to reset loadMore status
                isLoadingMore = false;
                isFinished = false;
                isWorking = false;
                setupUI(getActivity(), recyclerView);
                showHideList(true);
            } else {
                waitingTask.execute();
                waitingTask = null;
            }
        }

        @Override
        protected void onPreExecute() {
            isWorking = true;
            showHideList(false);
        }
    }
}
