package com.backbase.backbaseassignment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

public class MainActivity extends AppCompatActivity implements OnCityClickListener, OnMapReadyCallback {

    private boolean mapReady = false;
    CityModel waitingForMap;
    private GoogleMap googleMap;
    View mapLay;
    View listFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapLay = findViewById(R.id.mapsLay);
        listFragment = findViewById(R.id.frame);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapsFragment);

        mapFragment.getMapAsync(this);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, new CitiesListFragment())
                .commit();
    }


    private void initCamera(CityModel cityModel) {
        hideMap(true);
        CameraPosition cameraPosition = CameraPosition.builder()
                .target(new LatLng(cityModel.getCoord().getLat(),
                        cityModel.getCoord().getLon()))
                .zoom(12f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition), null);
    }

    @Override
    public void onCityClicked(CityModel cityModel) {
        if (mapReady) {
            initCamera(cityModel);
        } else {
            waitingForMap = cityModel;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_style));
        } catch (Resources.NotFoundException ignored) {
        }
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        mapReady = true;
        if (waitingForMap != null) {
            initCamera(waitingForMap);
        }
    }

    private void hideMap(final boolean showOrHide) {
        ObjectAnimator anim;
        if (showOrHide && mapLay.getVisibility() != View.VISIBLE) {
            anim = ObjectAnimator.ofFloat(mapLay, View.ALPHA, 0f, 1f);
        } else if (!showOrHide && mapLay.getVisibility() == View.VISIBLE) {
            anim = ObjectAnimator.ofFloat(mapLay, "alpha", 1f, 0f);
        } else {
            return;
        }
        anim.setDuration(100); // duration 3 seconds
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (showOrHide) {
                    mapLay.bringToFront();
                    mapLay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {

                if (!showOrHide) {
                    listFragment.bringToFront();
                    mapLay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        anim.start();
    }

    @Override
    public void onBackPressed() {
        if (mapLay.getAlpha() < 1) {
            super.onBackPressed();
        } else {
            hideMap(false);
        }
    }
}
