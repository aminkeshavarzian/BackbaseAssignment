package com.backbase.backbaseassignment;

import android.content.res.Resources;
import android.util.Log;

import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertFalse;

/**
 * Created by Amin on 21/03/2018.
 */

public class DataProviderUnitTest {

    @Test
    public void checkValidSearch() {
        DataProvider.setINSTANCE(new DataProvider(getStringFromJson()));
        assertFalse("List wasn't parsed correctly, Or data is Corrupted", DataProvider.getInstance().getCities().isEmpty());
        assertFalse("Search is not working correctly, Or data is Corrupted", DataProvider.getInstance().getCities("Esfahan", 0).isEmpty());
        assertFalse("Search is not working correctly, Or data is Corrupted", !DataProvider.getInstance().getCities("ASX", 0).isEmpty());
    }

    private String getStringFromJson() {
        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("cities.json");
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            return new String(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
}
