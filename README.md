##Introduction

this is the Assignment for Backbase Android Developer Position.

Github link: https://github.com/aminsepahan/BackbaseAssignment

My solution is to Convert the list to java object at once when the app loads ( since app load time was not important for the assignment) and query the list on demand (search and pagination) 
The Parsed list will be persistent in memory (which is not the best practice)

A couple of things to notice:

_ search will also search in country codes but only if they are exactly typed like "IR" for Iran 

_ default page size is 80 which can be easily changed with pageSize Constant in CitiesListFragment

_ As demanded in the assignment there is a unit test for search functionality, however since we have to read from the file, and resource files are not available in "Unit" test, I had to make a copy of it in test resources folder

_ I had only four hours for this, since its Persian New year season now, it like January 2nd and 3rd, so sorry for not being the Best I could
